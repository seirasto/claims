/**
 * 
 */
package claim;

import org.w3c.dom.*;
import java.util.*;
import blog.*;

/**
 * @author sara
 *
 */
public class ClaimReader {

	/**
	 * 
	 */
	public ClaimReader() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Read in XML File of claims
	 * Get pre-processing information of each claim (e.g. sentiment, committed belief)
	 * @param inputFile
	 * @return
	 */
	public static ArrayList<Object> read(String inputFile) { //, String processingDirectory, boolean CBtagger) {
		
		ArrayList<Object> claims = new ArrayList<Object>();
		HashMap<String,ArrayList<Object>> cs = readByPost(inputFile);
		
		Set<String> keys = cs.keySet();
		
		for (String key : keys) {
			claims.addAll(cs.get(key));
		}
		
		return claims;
	}
	
	/**
	 * Read in XML File of claims
	 * Get pre-processing information of each claim (e.g. sentiment, committed belief)
	 * @param inputFile
	 * @return
	 */
	public static HashMap<String,ArrayList<Object>> readByPost(String inputFile) { //, String processingDirectory, boolean CBtagger) {
		
		HashMap<String,ArrayList<Object>> claims = new HashMap<String,ArrayList<Object>>();
		
		Document claimFile = XMLUtilities.getDocument(inputFile);

		// entry
		NodeList entry = claimFile.getElementsByTagName("description");
		claims.put("-1",readClaims((Element)entry.item(0)));
		
		// comments
		NodeList comments = claimFile.getElementsByTagName("comment");
		
		for (int index = 0; index < comments.getLength(); index++) {
			claims.put(((Element)comments.item(index)).getAttribute("url"),readClaims((Element)comments.item(index)));
		}
		
		/*File f = new File(inputFile);
		
		//String processedFile = f.getParentFile().getParent() + "/temp/processed/processed-" + f.getName() + "/" + f.getName();
				
		String processedFile = processingDirectory + "/processed/processed-" + f.getName() + "/" + f.getName();
		
		// committed belief tagger (optional)
		/*List <String> beliefSentences = null;
		if (CBtagger) {
			beliefSentences = Belief.tagBelief(processedFile + ".emo.con");
		}
		
		List<String> chunkFile = Utils.readLines(processedFile + ".emo.con.pos.chk.emo");
		List<String> allSentences = Utils.readLines(processedFile);
		
		for (int j = 0; j < sentences.getLength(); j++) {
			for (int index = 0; index < allSentences.size(); index++) {		
				if (allSentences.get(index).equals(sentences.item(j).getTextContent())) {
					claims.add(new Claim(null,allSentences.get(index),null ,
							CBtagger && beliefSentences.size() > index ? new Belief(beliefSentences.get(index)) : null,
									chunkFile.get(index)));
					allSentences.remove(index);
					break;
				}
			}
		}*/
		return claims;
	}
	
	private static ArrayList<Object> readClaims(Element post) {
		ArrayList<Object> claims = new ArrayList<Object>();
		NodeList sentences = post.getElementsByTagName("sentence");
		
		for (int index = 0; index < sentences.getLength(); index++) {
			Element sentence = (Element)sentences.item(index);
			
			// is a claim
			if (sentence.getElementsByTagName("claim").getLength() > 0) {
				claims.add(sentence.getElementsByTagName("claim").item(0).getTextContent());
			}
			// if we want to keep aligned with document
			//else {
			//	claims.add(null);
			//}
		}
		return claims;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
