package claim;

import java.util.*;
import java.io.*;

import org.w3c.dom.*;

import blog.livejournal.Blog;
import blog.livejournal.Entry;
import processing.NlpUtils;

/**
 * Contains all the persuasion elements found in a blog entry
 * @author sara
 *
 */
public class Persuasion {

	String claim;
	int id;
	Hashtable<String,ArrayList<String>> support;
	List<String> claimSentences;
	
	public Persuasion(String claimIn, int idIn) {
		claim = claimIn;
		id = idIn;
		
		claimSentences = NlpUtils.splitSentences(claim);
		
		support = new Hashtable<String,ArrayList<String>>();
	}
	
	public static HashSet<String> getPersuasionText(Element entry) {
		Hashtable<Integer,Persuasion> persuasions = processEntryForPersuasion(entry);
		
		Iterator<Integer> it = persuasions.keySet().iterator();
		
		HashSet<String> persuasionText = new HashSet<String>();
		
		while (it.hasNext()) {
			Object key = it.next();
			persuasionText.add(persuasions.get(key).getClaim());
		}
		return persuasionText;
	}
	
	public static Hashtable<Integer,Persuasion> processEntryForPersuasion(Element entry) {
	
		Hashtable<Integer,Persuasion> persuasions = new Hashtable<Integer,Persuasion>();
		
		NodeList claims = entry.getElementsByTagName("claim");

		// setup persuasion by getting all claims in the entry *required
		for (int index = 0; index < claims.getLength(); index++) {
			
			Element claim = (Element)claims.item(index);
			
			Integer id = Integer.valueOf(claim.getAttribute("id"));
			String text = claim.getFirstChild().getNodeValue();
			
			persuasions.put(id,new Persuasion(text, id));
		}

		// add support - must contain at least one of these!
		Persuasion.processSupport(entry.getElementsByTagName("justification"),persuasions,"justification");
		Persuasion.processSupport(entry.getElementsByTagName("grounding"),persuasions,"grounding");
		Persuasion.processSupport(entry.getElementsByTagName("reiteration"),persuasions,"reiteration");
		// this support is optional
		Persuasion.processSupport(entry.getElementsByTagName("elaboration"),persuasions,"elaboration");
		
		return persuasions;
	}
	
	private static void processSupport(NodeList supportIn, Hashtable<Integer,Persuasion> persuasions, String type) {
		
		for (int index = 0; index < supportIn.getLength(); index++) {
		
			Element s = (Element)supportIn.item(index);
			
			Integer id = Integer.valueOf(s.getAttribute("id"));
			String text = s.getFirstChild().getNodeValue();
			
			persuasions.get(id).addSupport(type,text);
		}
	}
	
	public void addSupport(String type, String text) {
		ArrayList<String> supportText = support.get(type);
		
		if (supportText == null) supportText = new ArrayList<String>();
		
		supportText.add(text);
		support.put(type, supportText);
	}
	
	public String getClaim() {
		return claim;
	}


	public List<String> getClaimAsSentences() {
		return claimSentences;
	}
	
	public int getClaimID() {
		return id;
	}

	public ArrayList<String> getSupport(String type) {
		return support.get(type);
	}
	
	public boolean compareTo(String predictionText) {
		
		for (String sentence : claimSentences) {
			if (sentence.indexOf(predictionText) >= 0 || predictionText.indexOf(sentence) >= 0)
				return true;
		}
		
		return false;
	}
	
	public String toString() {
		String string = "";
		
		string += "claim " + this.id + ": " + this.claim + "\n";
		
		Iterator<String> supportIterator = support.keySet().iterator();
		
		while (supportIterator.hasNext()) {
			String type = (String)supportIterator.next();
			string += type + ": " + support.get(type) + "\n";
		}
		
		return string;
	}
	
	public static void main(String [] args) {
		
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter("/proj/nlp/users/sara/persuasion/claims/annotation/wikipedia-persuasion-claims.csv"));
			BufferedWriter sout = new BufferedWriter(new FileWriter("/proj/nlp/users/sara/persuasion/claims/input/wikipedia-persuasion-claims-sentences.csv"));
			
			File directory = new File("/proj/nlp/users/sara/persuasion/claims/annotated-wiki-persuasion/");
			
			for (File f : directory.listFiles()) {
				Blog b = null;
				try {
					b = Blog.processBlog(f.toString());
				} catch (Exception e) {
					continue;
				}
				
				if (b == null) continue;
				
				Entry e = (blog.livejournal.Entry)b.getEntry((String)b.getEntries().next());
				Hashtable<Integer,Persuasion> persuasions = Persuasion.processEntryForPersuasion(e.getEntryElement());
				
				Iterator<Integer> i = persuasions.keySet().iterator();
				
				while (i.hasNext())  {
					Persuasion p = persuasions.get(i.next());
					out.write(f.getName() + ",Y," +p.getClaim() + "\n");
					sout.write(p.getClaim() + "\n");
					
					List<String> support = new ArrayList<String>();
					if (p.getSupport("justification") != null) support.addAll(p.getSupport("justification"));
					if (p.getSupport("grounding") != null) support.addAll(p.getSupport("grounding"));
					if (p.getSupport("reiteration") != null) support.addAll(p.getSupport("reiteration"));
					if (p.getSupport("elaboration") != null) support.addAll(p.getSupport("elaboration"));
					
					for (String s : support) {
						out.write(f.getName() + ",N," + s + "\n");
						sout.write(s + "\n");
					}
				}
				
				List<String> sentences = processing.NlpUtils.splitSentences(e.getEntryText());
				
				Iterator<String> comments = e.getComments();
				
				while (comments.hasNext()) {
					Object url = comments.next();
					sentences.addAll(processing.NlpUtils.splitSentences(e.getComment(url.toString()).getCommentText()));
				}
				
				for (String sentence : sentences) {
					out.write(f.getName() + ",N," + sentence + "\n");
					sout.write(sentence + "\n");
				}
			}
			out.close();
			sout.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
