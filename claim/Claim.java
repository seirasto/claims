package claim;

import edu.columbia.opinion.io.*;
import java.text.DecimalFormat;
import java.util.*;

import social_media.Features;

/**
 * A claim is part of persuasion. It is computed as sentiment+belief
 * @author sara
 *
 */
public class Claim {
	String _sentence;
	String _id;
	boolean _claim;
	boolean _subjective;
	boolean _cb;
	boolean _annotated;
	Belief _committedBelief;
	ResultSentence _sentiment;
	ResultSentence _generatedSentiment;
	double _confidence;
	List<String> _pos;
	String _domain;
	//String _chunkSentence;
	
	public Claim(String id, String sentence, ResultSentence sentiment, Belief committedBelief, String pos, String domain) {
		_sentiment = sentiment;
		
		if (committedBelief != null) {
			_committedBelief = committedBelief;
			_cb = committedBelief.isBelief();
		}
		
		_id = id;
		_sentence = sentence.startsWith("\"") && sentence.endsWith("\"") ? sentence.substring(1,sentence.length()-1) : sentence;
		_annotated = false;
		
		if (pos != null) {
			//_chunkSentence = chunk;
			//generatePOSSentence(chunk);
			_pos = stripText(pos);
		}
		_domain = domain;
	}
	
	public Claim(String id, String sentence, String pos, String domain) {
		this(id,sentence,null,null,pos, domain);
	}
	
	public Claim(String id, String sentence, boolean claim, String sentiment, Belief cb, String pos, String domain) {
		this(id,sentence,sentiment == null ? null : SentimentResult.processAnnotatedSentence(sentence, 
				sentiment != null && sentiment.startsWith("\"") && sentiment.endsWith("\"") ? sentiment.substring(1,sentiment.length()-1) : sentiment),cb,pos, domain);
		_claim = claim;
		_annotated = true;
		
	}
	
	public void setId(String id) {
		_id = id;
	}
	
	public String getDomain() {
		return _domain;
	}
	
	public String getPOS(int index) {
		return _pos.get(index);
	}
	
    public List<String> stripText(String text) {
    	  
    	if (text == null) return null;
    	
		String [] words = text.split(" ");
		String pos = "";
		
		for (int index = 0; index < words.length; index++) {
			String posTag = words[index].substring(words[index].lastIndexOf("/") + 1);
			pos += posTag + " ";
		}
		return Arrays.asList(pos.split("\\s+"));
    }
	
	/*public String getChunkSentence() {
		return _chunkSentence;
	}*/
	
	public boolean hasPronoun() {
		
		for (String pos : _pos) {
			if (pos.matches("PRP.*")) {
				return true;
			}
		}
		return false;
	}
	
	public String getPOS() {
		
		String sentence = "";
		
		for (String pos : _pos) {
			sentence += " " + pos;
		}
		return sentence.trim();
	}
	
	/*private void generatePOSSentence(String chunkSentence) {

		String [] chunks = chunkSentence.replaceAll("\"","").split(" ");
		_pos = new ArrayList<String>();
		
		int size = _sentence.split(" ").length;
		
		for (int index = 0; index < chunks.length && index < size; index++) {
			String deChunked = chunks[index].substring(0,chunks[index].lastIndexOf("/"));
			String posTag = deChunked.substring(deChunked.lastIndexOf("/") + 1);
			
			_pos.add(posTag);
		}
	}*/
	
	public boolean isClaim() {
		return _claim;
	}
	
	public String getID() {
		return _id;
	}
	
	public String getSentence() {
		return _sentence;
	}
	
	public double getConfidence() {
		return _confidence;
	}
	
	public String getSentiment(boolean generated) {
		ResultSentence sentiment = generated ? _generatedSentiment : _sentiment;
		
		if (sentiment == null) return null;
		return sentiment.getSentenceSubjectivity().replaceAll("\\s+", " ");
	}
	
	public void addGeneratedSentiment(ResultSentence sentiment) {
		_generatedSentiment = sentiment;
	}
	
	public double getSentimentCount(boolean generated, boolean normalized) {
		
		ResultSentence sentiment = generated ? _generatedSentiment : _sentiment;
		
		if (sentiment == null) return -1;
		
		double count = 0;
		
		List<Phrase> phrases = sentiment.getPhrases();
		
		for (Phrase phrase : phrases) {
			if (phrase.sentiment().equals("s")) count++;
		}
		
		return normalized ? count/phrases.size() : count;
	}
	
	public boolean hasSentiment(boolean generated) {
		ResultSentence sentiment = generated ? _generatedSentiment : _sentiment;
		
		List<Phrase> phrases = sentiment.getPhrases();
		
		for (Phrase phrase : phrases) {
			if (phrase.sentiment().equals("s")) return true;
		}
		return false;
	}
	
	public String getSubjectiveWords(boolean generated) {
    	
		ResultSentence sentiment = generated ? _generatedSentiment : _sentiment;
		
    	String words = "";
    	
    	for (Phrase phrase : sentiment.getPhrases()) {
    			if (phrase.sentiment().equals("s")) words += phrase.getPhrase() + " ";
    	}
    	return words.trim();
    }
	
	public boolean endsInQuestion() {
		return _sentence.endsWith("?");
	}
	
	public boolean compareTo(Claim claim) {
		return (_claim == claim.isClaim()); 
	}
	
	public boolean isAnnotated() {
		return _annotated;
	}
	
	public Belief getCommittedBelief() {
		return _committedBelief;
	}
	
	public int length() {
		return _sentence.split("\\s+").length;
	}
	
	public Hashtable<String,Double> getSocialMediaFeatures(Features socialMedia) {
    	return socialMedia.compute(_sentence.split(" "));
	}
	
	public String toString() {
		
		String string =   _id + "," + (_claim ? "Y" : "N") + ",";
		
		if (_sentiment != null) {
			string += _sentiment.toString().replaceAll(",", "");
			return string;
		}
		return string /*+ " " + (_annotated == true ? "(Ann)" : "(Sys)" ) + " " */ + new DecimalFormat("0.0%").format(_confidence) + _sentence.replaceAll(",","");
	}	 
}