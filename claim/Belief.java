package claim;

import java.util.Properties;
import java.io.*;
//import java.util.regex.*;
import java.util.ArrayList;

import processing.GeneralUtils;

/**
 * Belief Tags: B-CB, B-NA, B-NCB
 * @author sara
 *
 */
public class Belief {

	int _cb;
	int _na;
	int _ncb;
	String _sentence;
	ArrayList<String> _belief;
	
	public Belief(String sentence) {
		_sentence = sentence;
		_cb = 0;
		_na = 0;
		_ncb = 0;
		
		/*Pattern p = Pattern.compile("/B-CB");
		Matcher m = p.matcher(_sentence);
		while (m.find()) _cb++;
		p = Pattern.compile("/B-NA");
		m = p.matcher(_sentence);
		while (m.find()) _na++;
		p = Pattern.compile("/B-NCB");
		m = p.matcher(_sentence);
		while (m.find()) _ncb++;*/
		
		_belief = new ArrayList<String>();
	
		String [] words = _sentence.split("\\s+");
		
		for (String taggedWord : words) {
			
			int backslash = taggedWord.lastIndexOf("/");
			String word = taggedWord;
			String tag = "";

			if (backslash > 0) {
				word = taggedWord.substring(0,backslash);
				tag = taggedWord.substring(backslash + 1);
			}
		
				
			if (!tag.isEmpty()) {
				if (tag.equals("B-CB")) _cb++;
				else if (tag.equals("B-NA")) {
					_na++;
					continue;
				}
				else if (tag.equals("B-NCB")) _ncb++;
				
				_belief.add(word);
			}
			
		}
	}
	
	public boolean isBelief() {
		if (_cb > 0) return true;
		return false;
	}
	
	public boolean isNCBelief() {
		if (_ncb > 0) return true;
		return false;
	}
	
	public double getConfidence() {
		double confidence = .65;
		confidence += (_cb * .05);
		return confidence;
	}
	
	public int getCB() {
		return _cb;
	}
	
	public int getNCB() {
		return _ncb;
	}
	
	public int getNA() {
		return _na;
	}
	
	public int getBelief() {
		return _na + _cb + _ncb;
	}
	
	public String toString() {
		return "cb: " + _cb + " ncb:" + _ncb + " na: " + _na;
	}
	
	public String getBeliefWords() {
		String words = "";

		for (String belief : _belief) {
			words += belief + " ";
		}
		return words.trim();	
	}

	public static ArrayList<String> tagBelief(String inputFile) {
		ArrayList<String> sentences = new ArrayList<String>();
		
		try {
			if (!new File(inputFile + ".cb").exists()) {
				// tag the file
				Properties prop = GeneralUtils.getProperties("config/config.properties");
				String belief = prop.getProperty("belief");
				
				Runtime rt = Runtime.getRuntime();
				Process process = rt.exec("/bin/bash " + belief + "/Run.sh " + inputFile);
				InputStream stderr = process.getErrorStream();
	            InputStream stdout = process.getInputStream();

	            BufferedReader br = new BufferedReader(new InputStreamReader(stderr));
	            String line = null;
	            
	            while ( (line = br.readLine()) != null)
	                System.err.println(line);

				process.waitFor();
				stderr.close();
				stdout.close();
			}
			
			// read the file
			BufferedReader in
			   = new BufferedReader(new FileReader(inputFile + ".cb"));
			String sentence;
			
			while ((sentence = in.readLine()) != null) {
				sentences.add(sentence);
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return sentences;
	}
}
