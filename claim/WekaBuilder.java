package claim;

import java.io.*;
import java.util.*;

import processing.GeneralUtils;

import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SelectedTag;
import weka.core.converters.ArffSaver;
import weka.filters.supervised.attribute.AttributeSelection;
import weka.filters.unsupervised.attribute.StringToWordVector;

import social_media.Features;
import edu.columbia.opinion.io.*;
import edu.columbia.utilities.weka.*;
import features.FeatureSelection;

public class WekaBuilder {

	protected static final String TEXT = "text_string";
	protected static final String POS = "pos_string";
	protected static final String SENTIMENT = "sentiment_string";
	protected static final String HAS_SENTIMENT = "has_sentiment";
	protected static final String SENTIMENT_COUNT = "sentiment_count";
	protected static final String QUESTION_END = "ends_in_question";
	protected static final String PRP = "has_pronoun";
	protected static final String QUOTES = "has_quotes";
	protected static final String SENTENCE_LENGTH = "sentence_length";
	protected static final String HAS_BELIEF = "has_belief";
	protected static final String HAS_NCBELIEF = "has_nc_belief";
	protected static final String BELIEF_COUNT = "belief";
	protected static final String NCBELIEF_COUNT = "nc_belief_count";
	protected static final String BELIEF = "belief_string";
    protected static final String N_WORDS = "wordCount";
    protected static final String CAP_WORDS = "capitalWords";
    protected static final String SLANG = "slang";
    protected static final String EMOTICONS = "emoticons";
    protected static final String ACRONYMS = "acronyms";
    protected static final String PUNCTUATION = "punctuation";
	protected static final String PUNCTUATION_R = "repeated_punctuation";
	protected static final String PUNCTUATION_NUM = "punctuation_count";
	protected static final String EXCLAMATION = "!";
	protected static final String EXCLAMATION_R = "!!!";
	protected static final String QUESTION = "?";
	protected static final String QUESTION_R = "???";
	protected static final String ELLIPSES = "...";
    protected static final String LINKS_IMAGES = "linksImages";
    protected static final String WORD_LENGTHENING = "loooooong";
    protected static final String DOMAIN = "domain";
    String [] _sentiment = {"o","s","o o","o s","s o","s s","o o s","o o o","o s o","o s s","s o s","s o o","s s o","s s s"};
    ArrayList<Integer> _featureRanges;
    ArrayList<String> _featureNames;
    
	String _outputDir;
	boolean _overwrite;
	boolean _socialMedia;
	boolean _normalized;
	boolean _CBtagger;
	boolean _opinion;
	boolean _POS;
	boolean _question; 
	boolean _featureSelection;
	boolean _generatedSentiment = true;
	List<String> _domain;
	
	protected StringToWordVector _textNgramFilter;
	protected StringToWordVector _counterFilt;
	protected StringToWordVector _beliefFilt;
	protected StringToWordVector _sentimentFilt;
	protected AttributeSelection _chiSquareFilter;
	
	HashSet<String> _nGrams;
	HashSet<String> _pos;
	HashSet<String> _belief;
	HashSet<String> _sentimentTerms;
	//HashSet<String> _sentimentChunkGrams;
    int _nGramSize = 250; // default
    Features _socialMediaFeatures;

	
	public WekaBuilder(String outputDirectory, boolean overwrite, boolean question, boolean socialMedia, 
			boolean normalized, boolean CBtagger, boolean opinion, int nGramSize, boolean usePOS, boolean featureSelection, List<String> domains) {
		_outputDir = outputDirectory;
		_overwrite = overwrite;
		_socialMedia = socialMedia;
		_normalized = normalized;
		_CBtagger = CBtagger;
		_opinion = opinion;
		_question = question;
		_nGramSize = nGramSize;
		_POS = usePOS;
		_featureSelection = featureSelection;
		_domain = domains;
		
		if (_socialMedia) {
	    	
	    	String emoticonDirectory = null; 
	    	String dictionary = null;
	    	
	    	try {
	    		Properties prop = GeneralUtils.getProperties("config/config.properties");
				emoticonDirectory = prop.getProperty("emoticons_directory");
				dictionary = prop.getProperty("dictionary");
			} catch (Exception e) {
				e.printStackTrace();
			}
	    	_socialMediaFeatures = new Features(dictionary,emoticonDirectory + "emoticon_sub.txt", emoticonDirectory + "acronyms.txt");
		}
	}

	/*protected Instances claimsToInstances(String name, FastVector attVector, List<Claim> claims, List<SentimentResult> results, List<String> ids) {
		
		Instances instances = new Instances(name, attVector, 0);
		
		int index = 0;
		
		for (Claim claim : claims) {
			
			String sentiment = "";
			
			if (results != null) {
							
				while (index < ids.size() && ids.get(index).equals(claim.getID())) {
					sentiment += results.get(index).toStringPhrase(0);
					index++;
				}
			}
			
			instances.add(claimToInstance(claim,instances, results == null ? null : new Claim(null,claim.getSentence(),
					SentimentResult.ProcessLabeledSentence(sentiment),claim.getCommittedBelief(),claim.getPOS(),claim.getDomain())));
		}
		
		// if (results != null && index < results.size()) {
		// System.out.println(results.get(index));
		//}
		
		return instances;
	}*/
	
	protected Instances claimsToInstances(String name, ArrayList<Attribute> attVector, List<Claim> claims, List<ResultSentence> opinions) {
		
		Instances instances = new Instances(name, attVector, 0);
		instances.setClass(instances.attribute("claim"));
		double [] belief = {0,0};
		for (int index = 0; index < claims.size(); index++) {
			Instance instance = claimToInstance(claims.get(index),instances);
			belief[(int)instance.value(instances.attribute("claim"))] += instance.value(instances.attribute("-be_" + BELIEF_COUNT)) > 0 ? 1 : 0;
			instances.add(instance);
		}
		
		System.out.println(Arrays.toString(belief) + "(" + instances.numInstances() + ")");
		return instances;
	}
	
	protected Instances makeTestDataset(List<Claim> claims, List<ResultSentence> opinion) {
		
		Instances test = null;
		
		try {
			ArrayList<Attribute> attVector = getAttributes(false);
			
			for (int index = 0; index < claims.size(); index++) {
				
				if (opinion != null) {
					 claims.get(index).addGeneratedSentiment(opinion.get(index));
				}
			}
			
			// don't use gold
			test = claimsToInstances("test set", attVector, claims, opinion);	
			ArffSaver saver = new ArffSaver();
	    	saver.setInstances(test);
	    	saver.setFile(new File(_outputDir + "claim-all-test.arff"));
	    	saver.writeBatch();
			
			//int size = test.numAttributes();

			if (_nGramSize > 0) {
		        // generate text n-grams
		        test = ChiSquareFeatureSelection.applyTestFilter(test, _textNgramFilter, _nGrams, "-ngram", "claim"); //chiSquareTest(test,_nGrams,size-1);
		        //size = test.numAttributes();
			}
			if (_opinion) {				
		        // generate Sentiment counts
		        test = ChiSquareFeatureSelection.applyTestFilter(test, _sentimentFilt, _sentimentTerms, "-opinion","claim"); //chiSquareTest(test,_sentimentTerms,size-1);
			}
			if (_POS) {				
		        // generate POS counts
		        test = ChiSquareFeatureSelection.applyTestFilter(test, _counterFilt, _pos, "-pos","claim"); //chiSquareTest(test,_pos,size-1);
			}
	        if (_CBtagger) {
		        //size = test.numAttributes();
		       test = ChiSquareFeatureSelection.applyTestFilter(test, _beliefFilt, _belief, "-belief","claim"); //chiSquareTest(test,_belief,size-1);	        
	        }
	    	test.setClass(test.attribute("claim"));
	        // put class last
	        //test = InstancesProcessor.reorder(test);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}	
		return test;
	}
		
	protected Instances makeTrainDataset(List<Claim> claims, List<ResultSentence> opinion) {
		_featureRanges = new ArrayList<Integer>();
		_featureNames = new ArrayList<String>();
		ArrayList<Attribute> attVector = getAttributes(true);
		Instances train = null;
		
		for (int index = 0; index < claims.size(); index++) {
			
			if (opinion != null) {
				 claims.get(index).addGeneratedSentiment(opinion.get(index));
			}
		}
		
		try {
			// already created
			if (new File(_outputDir + "/claim.arff").exists() && !_overwrite) {
				//System.out.println("Loading: " + _outputFile);
				BufferedReader claimReader = new BufferedReader(new FileReader(_outputDir + "/claim.arff"));
				train = new Instances(claimReader);
				claimReader.close();
			}
			else {
				
				train = claimsToInstances("train set", attVector, claims, opinion);
				
				// save instances so that it doesn't have to be generated from scratch again
		    	if (!new File(_outputDir + "/claim.arff").exists() || _overwrite) {
			    	ArffSaver saver = new ArffSaver();
			    	saver.setInstances(train);
			    	saver.setFile(new File(_outputDir + "claim.arff"));
			    	saver.writeBatch();
		    	}
			}
			chiSquare(claims);
			train.setClass(train.attribute("claim"));
			train = filter(train);
			train.setClass(train.attribute("claim"));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return train;
	}
	
	private void chiSquare(List<Claim> claims) {
		
		// sentences
		HashMap<String,List<String>> sentences = new HashMap<String,List<String>>();
		sentences.put("N", new ArrayList<String>());
		sentences.put("Y", new ArrayList<String>());
		// belief
		HashMap<String,List<String>> beliefs = new HashMap<String,List<String>>();
		beliefs.put("N", new ArrayList<String>());
		beliefs.put("Y", new ArrayList<String>());
		// sentiment
		HashMap<String,List<String>> sentiments = new HashMap<String,List<String>>();
		sentiments.put("N", new ArrayList<String>());
		sentiments.put("Y", new ArrayList<String>());
		
		for (Claim claim : claims) {
			String type = claim.isClaim() ? "Y" : "N";

			String sentence = claim.getSentence();
			sentences.get(type).add(sentence);
			
			String belief = claim.getCommittedBelief().getBeliefWords();
			beliefs.get(type).add(belief);

			String sentiment = claim.getSubjectiveWords(_generatedSentiment);
			sentiments.get(type).add(sentiment);

		}
		if (_featureSelection) {
			_nGrams = unique(FeatureSelection.chiSquare(sentences, 3, 250, 3.84,true),"-ngram");
			//System.out.println(Arrays.toString(_nGrams.toArray()));
			_sentimentTerms = unique(FeatureSelection.chiSquare(sentiments, 1, 250, 3.84,true),"-opinion");
			//System.out.println(Arrays.toString(_sentimentTerms.toArray()));
			_belief = unique(FeatureSelection.chiSquare(beliefs, 1, 250, 3.84,true),"-belief");
			//System.out.println(Arrays.toString(_belief.toArray()));
		}
	}
	
	private HashSet<String> unique(HashMap<String, List<String>> terms, String type) {
		HashSet<String> unique = new HashSet<String>();
		

		
		for (String c : terms.keySet()) {
			System.out.println(c + ": " + Arrays.toString(terms.get(c).toArray()));
			//System.out.println(type + c + ": " + Arrays.toString(terms.get(c).toArray()));
			for (String w : terms.get(c)) {
				unique.add(type + "_" + w);
			}
		}
		return unique;
	}
 	
	private StringToWordVector generateNgramFilt(boolean normalize) {
		//Stemmer stem = new SnowballStemmer();
		
		StringToWordVector ngramFilt = new StringToWordVector();
        //ngramFilt.setStemmer(stem);
        ngramFilt.setLowerCaseTokens(true);
        ngramFilt.setWordsToKeep(1000000);
        //ngramFilt.setOutputWordCounts(true);        
        //ngramFilt.setUseStoplist(true);
        //ngramFilt.setDoNotOperateOnPerClassBasis(newDoNotOperateOnPerClassBasis);
        if (normalize) ngramFilt.setNormalizeDocLength(new SelectedTag(StringToWordVector.FILTER_NORMALIZE_ALL, StringToWordVector.TAGS_FILTER));
        
        return ngramFilt;
	}
	
	protected Instances filter(Instances train) throws Exception {
		
		boolean printStats = false;
		boolean normalize = true;
	
        List<Object> modified = null;
		if (_nGramSize > 0) {
        	modified = ChiSquareFeatureSelection.adjustNames(train, generateNgramFilt(normalize), _textNgramFilter, 
        			new int[] {train.attribute(TEXT).index()}, "-ngram", 1, 3, normalize, "claim");
        	train = (Instances)modified.get(0); _textNgramFilter = (StringToWordVector)modified.get(1);
	        if (_featureSelection) {
	        	train = deleteChiSquareAttributes(train,_nGrams,"-ngram_");
	        	//modified = ChiSquareFeatureSelection.applyFilter(train, "-ngram", "claim", printStats);
		        //train = (Instances)modified.get(0); _nGrams = (HashSet<String>)modified.get(1);
	        }
	        else _nGrams = ChiSquareFeatureSelection.setNgramsByName(train, "-ngram", printStats);
        }
		if (_opinion) {
	        modified = ChiSquareFeatureSelection.adjustNames(train, generateNgramFilt(normalize), _sentimentFilt, 
	        		new int [] {train.attribute(SENTIMENT).index()}, "-opinion", 1, 1, normalize, "claim");
        	train = (Instances)modified.get(0); _sentimentFilt = (StringToWordVector)modified.get(1);
	        
        	if (_featureSelection) {
	        	train = deleteChiSquareAttributes(train,_sentimentTerms,"-opinion_");
        		//modified = ChiSquareFeatureSelection.applyFilter(train, "-opinion", "claim", printStats);
        		//train = (Instances)modified.get(0); _sentimentTerms = (HashSet<String>)modified.get(1);
        	}
        	else _sentimentTerms = ChiSquareFeatureSelection.setNgramsByName(train, "-opinion", printStats);
		}
		if (_POS) {
			modified = ChiSquareFeatureSelection.adjustNames(train, generateNgramFilt(normalize), _counterFilt, 
					new int[] {train.attribute(POS).index()}, "-pos", 1, 1, normalize, "claim");
        	train = (Instances)modified.get(0); _counterFilt = (StringToWordVector)modified.get(1);
	        //modified = ChiSquareFeatureSelection.applyFilter(train, "-pos");
	        //train = (Instances)modified.get(0); _pos = (HashSet<String>)modified.get(1);
        	_pos = ChiSquareFeatureSelection.setNgramsByName(train, "-pos", printStats);
		}
        if (_CBtagger) {
        	modified = ChiSquareFeatureSelection.adjustNames(train, generateNgramFilt(normalize), _beliefFilt, 
        			new int[] {train.attribute(BELIEF).index()}, "-belief", 1, 1, normalize, "claim");
        	train = (Instances)modified.get(0); _beliefFilt = (StringToWordVector)modified.get(1);
	        
        	if (_featureSelection) {
	        	train = deleteChiSquareAttributes(train,_belief,"-belief_");
        		//modified = ChiSquareFeatureSelection.applyFilter(train, "-belief", "claim", printStats);
        		//train = (Instances)modified.get(0); _belief = (HashSet<String>)modified.get(1);
        	}
        	else _belief = ChiSquareFeatureSelection.setNgramsByName(train, "-belief", printStats);
        }
        	
        // put class last
        //train = InstancesProcessor.reorder(train);
        
        return train; 
	}
	
	protected Instances deleteChiSquareAttributes(Instances train, HashSet<String> grams, String name) {
		
		String list = "";
		
		HashSet<String> notfound = new HashSet<String>(grams);
    	for (int index = 0; index < train.numAttributes(); index++) {
    		if (!train.attribute(index).name().startsWith(name)) continue;
    		if (grams.contains(train.attribute(index).name())) {
    			notfound.remove(train.attribute(index).name());
    			continue;
    		}
    		list += "," + (index+1);
    	}
    	
    	try {
    		train = InstancesProcessor.removeList(train,list.substring(1));
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    	
    	for (String word : notfound) {
    		grams.remove(word);
    		//System.out.print(word + " ");
    	}
    	//System.out.println("");
    	return train;
	}
	
	protected ArrayList<Attribute> getAttributes(boolean update) {
		
		ArrayList<Attribute> attVector = new ArrayList<Attribute>();
        
        attVector.add(InstancesProcessor.nominalAtt("claim", new String [] {"Y","N"}));
        
        if (_domain != null) {
        	String [] domains = new String[_domain.size()];
        	domains = _domain.toArray(domains);
    		attVector.add(InstancesProcessor.nominalAtt(DOMAIN, domains));
        }
	    
        if (_nGramSize > 0) {
        	attVector.add(InstancesProcessor.stringAtt(TEXT));
        	if (update) _featureNames.add("-ngram");
        }

        if (_opinion) {
            attVector.add(InstancesProcessor.stringAtt(SENTIMENT));
            if (update) _featureNames.add("-opinion");
        }
        
        if (_POS) {
        	attVector.add(InstancesProcessor.stringAtt(POS));
        	if (update) _featureNames.add("-pos");
        }
        
        if (_CBtagger) {
        	attVector.add(InstancesProcessor.stringAtt(BELIEF));
        	if (update) _featureNames.add("-belief");
        }
        
        if (_question) {
	        attVector.add(InstancesProcessor.numericAtt("-?_" + QUESTION_END));
	       // if (update) _featureRanges.add(1);
	        if (update) _featureNames.add("-?");
        }
        
        if (_opinion) {
            
	        /*for (int index = 0; index < _sentiment.length; index++) {
	        	attVector.add(InstancesProcessor.numericAtt("-op_" + _sentiment[index]));
	        }*/
	        attVector.add(InstancesProcessor.numericAtt("-op_" + HAS_SENTIMENT));
	        attVector.add(InstancesProcessor.numericAtt("-op_" + SENTIMENT_COUNT));
        }
        
        if (_CBtagger) {
        	//attVector.add(numericAtt(HAS_BELIEF));
        	attVector.add(InstancesProcessor.numericAtt("-be_" + BELIEF_COUNT));
        	//attVector.add(numericAtt(NCBELIEF_COUNT));
        }
        
        /*
         * Social Media Features:
         * 
         * Sentence Length, Capital Words, Slang, Emoticons, Acronyms, Punctuation
         * 
         */
        if (_socialMedia) {
	        //attVector.add(Utils.numericAtt(N_WORDS));
	        attVector.add(InstancesProcessor.numericAtt("-sm_" + CAP_WORDS));
	        attVector.add(InstancesProcessor.numericAtt("-sm_" + SLANG));
	        attVector.add(InstancesProcessor.numericAtt("-sm_" + EMOTICONS));
	        attVector.add(InstancesProcessor.numericAtt("-sm_" + ACRONYMS));
	        attVector.add(InstancesProcessor.numericAtt("-sm_" + PUNCTUATION));
	    	attVector.add(InstancesProcessor.numericAtt("-sm_" + PUNCTUATION_R));
	    	attVector.add(InstancesProcessor.numericAtt("-sm_" + PUNCTUATION_NUM));
	    	attVector.add(InstancesProcessor.numericAtt("-sm_" + EXCLAMATION));
	    	attVector.add(InstancesProcessor.numericAtt("-sm_" + EXCLAMATION_R));
	    	attVector.add(InstancesProcessor.numericAtt("-sm_" + QUESTION));
	    	attVector.add(InstancesProcessor.numericAtt("-sm_" + QUESTION_R));
	    	attVector.add(InstancesProcessor.numericAtt("-sm_" + WORD_LENGTHENING));
	    	attVector.add(InstancesProcessor.numericAtt("-sm_" + LINKS_IMAGES));
	    	attVector.add(InstancesProcessor.numericAtt("-sm_" + ELLIPSES));
	    	attVector.add(InstancesProcessor.numericAtt("-sm_" + QUOTES));
	        attVector.add(InstancesProcessor.numericAtt("-sm_" + SENTENCE_LENGTH));
	    	//if (update) _featureRanges.add(12);
	    	if (update) _featureNames.add("-sm");
       }

       return attVector;
	}
	
	protected Instance claimToInstance(Claim claim, Instances instances) {
        
        Instance instance = new DenseInstance(1 + (_question ? 1 : 0) + (_POS ? 1 : 0) + (_nGramSize > 0 ? 1 : 0)  + (_opinion ? (/*_sentiment.length + */ 3) : 0) + 
        		(_socialMedia ? 16 : 0) + (_CBtagger ? 2 : 0) + (_domain != null ? 1 : 0));
        
        if (_question) {
	        instance.setValue(instances.attribute("-?_" + QUESTION_END),claim.endsInQuestion() ? 1 : 0);
        }
        
        if (_nGramSize > 0) {
        	instance.setValue(instances.attribute(TEXT),claim.getSentence());
        }
        
        if (_POS) {
        	instance.setValue(instances.attribute(POS),claim.getPOS());
        }
        
        // sometimes the belief tagger fails!
        if (_CBtagger && claim.getCommittedBelief() != null) {

        	//instance.setValue(instances.attribute(HAS_BELIEF),claim.getCommittedBelief().isBelief() ? 1 : 0);
        	instance.setValue(instances.attribute("-be_" + BELIEF_COUNT),(claim.getCommittedBelief().getCB()+claim.getCommittedBelief().getNCB())/(double)claim.length());
        	//instance.setValue(instances.attribute(NCBELIEF_COUNT),claim.getCommittedBelief().getNCB()/claim.length());
        	instance.setValue(instances.attribute(BELIEF),claim.getCommittedBelief().getBeliefWords());
        }
        
        if (_opinion) {
	        /*for (int index = 0; index < _sentiment.length; index++) {
	        	instance.setValue(instances.attribute("-op_" + _sentiment[index]),claim.getSentiment(_generatedSentiment).indexOf(_sentiment[index]) >= 0 ? 1 : 0);
	        }*/
		    
	        instance.setValue(instances.attribute("-op_" + HAS_SENTIMENT),claim.hasSentiment(_generatedSentiment) ? 1 : 0);
	        instance.setValue(instances.attribute("-op_" + SENTIMENT_COUNT),claim.getSentimentCount(_generatedSentiment, _normalized));
	        instance.setValue(instances.attribute(SENTIMENT), claim.getSubjectiveWords(_generatedSentiment));
        }
                      
        // social media features
        if (_socialMedia) {
        	Hashtable<String,Double> features = claim.getSocialMediaFeatures(_socialMediaFeatures);
        	
        	//  Num Words, Capital Words, %, Slang, %, :), %, LOL, %, ., %, .+$#@, %, Punctuation Count, %, !, %, !!!, %, ?, %, ???, %, ..., % 
        	double numWords = features.get(Features.NUM_WORDS);
        	/*System.out.println((gold.isClaim() ? "Y" : "N") + "," + features.get(Features.NUM_WORDS) 
        		+ "," + features.get(Features.CAPITAL_WORDS) + "," + features.get(Features.CAPITAL_WORDS)/numWords
        		+ "," + features.get(Features.SLANG) + "," + features.get(Features.SLANG)/numWords
        		+ "," + features.get(Features.EMOTICONS) + "," + features.get(Features.EMOTICONS)/numWords
        		+ "," + features.get(Features.ACRONYMS) + "," + features.get(Features.ACRONYMS)/numWords
        		+ "," + features.get(Features.PUNCTUATION) + "," + features.get(Features.PUNCTUATION)/numWords
        		+ "," + features.get(Features.PUNCTUATION_R) + "," + features.get(Features.PUNCTUATION_R)/numWords
        		+ "," + features.get(Features.PUNCTUATION_NUM) + "," + features.get(Features.PUNCTUATION_NUM)/numWords
        		+ "," + features.get(Features.EXCLAMATION) + "," + features.get(Features.EXCLAMATION)/numWords
        		+ "," + features.get(Features.EXCLAMATION_R) + "," + features.get(Features.EXCLAMATION_R)/numWords
        		+ "," + features.get(Features.QUESTION) + "," + features.get(Features.QUESTION)/numWords
        		+ "," + features.get(Features.QUESTION_R) + "," + features.get(Features.QUESTION_R)/numWords
        		+ "," + features.get(Features.ELLIPSES) + "," + features.get(Features.ELLIPSES)/numWords);*/
        	
            //instance.setValue(dataset.attribute(N_WORDS), features.get(Features.NUM_WORDS));
        	instance.setValue(instances.attribute("-sm_" + WORD_LENGTHENING), _normalized ? features.get(Features.WORD_LENGTHENING)/numWords : features.get(Features.WORD_LENGTHENING));
        	instance.setValue(instances.attribute("-sm_" + LINKS_IMAGES), _normalized ? features.get(Features.LINKS)/numWords : features.get(Features.LINKS));
        	instance.setValue(instances.attribute("-sm_" + CAP_WORDS), _normalized ? features.get(Features.ALL_CAPS_WORDS)/numWords : features.get(Features.ALL_CAPS_WORDS));
            instance.setValue(instances.attribute("-sm_" + SLANG), _normalized ? features.get(Features.SLANG)/numWords : features.get(Features.SLANG));
            instance.setValue(instances.attribute("-sm_" + EMOTICONS), _normalized ? features.get(Features.EMOTICONS)/numWords : features.get(Features.EMOTICONS));
            instance.setValue(instances.attribute("-sm_" + ACRONYMS), _normalized ? features.get(Features.ACRONYMS)/numWords : features.get(Features.ACRONYMS));
            instance.setValue(instances.attribute("-sm_" + PUNCTUATION), _normalized ? features.get(Features.PUNCTUATION)/numWords : features.get(Features.PUNCTUATION));
            instance.setValue(instances.attribute("-sm_" + PUNCTUATION_R), _normalized ? features.get(Features.PUNCTUATION_R)/numWords : features.get(Features.PUNCTUATION_R));
	    	instance.setValue(instances.attribute("-sm_" + PUNCTUATION_NUM), _normalized ? features.get(Features.PUNCTUATION_NUM)/numWords : features.get(Features.PUNCTUATION_NUM));
	    	instance.setValue(instances.attribute("-sm_" + EXCLAMATION), _normalized ? features.get(Features.EXCLAMATION)/numWords : features.get(Features.EXCLAMATION));
	    	instance.setValue(instances.attribute("-sm_" + EXCLAMATION_R), _normalized ? features.get(Features.EXCLAMATION_R)/numWords : features.get(Features.EXCLAMATION_R));
	    	instance.setValue(instances.attribute("-sm_" + QUESTION), _normalized ? features.get(Features.QUESTION)/numWords : features.get(Features.QUESTION));
	    	instance.setValue(instances.attribute("-sm_" + QUESTION_R), _normalized ? features.get(Features.QUESTION_R)/numWords : features.get(Features.QUESTION_R));
	    	instance.setValue(instances.attribute("-sm_" + ELLIPSES), _normalized ? features.get(Features.ELLIPSES)/numWords : features.get(Features.ELLIPSES));
	    	instance.setValue(instances.attribute("-sm_" + SENTENCE_LENGTH),claim.getSentence().length());
	        instance.setValue(instances.attribute("-sm_" + QUOTES), claim.getSentence().matches(".*['\"`].*['\"`].*") ? 1 : 0);
        }
        
        // set class
        instance.setValue(instances.attribute("claim"),claim.isClaim() ? "Y" : "N");
        if (_domain != null) instance.setValue(instances.attribute(DOMAIN), claim.getDomain());

        return instance;
    }
}
