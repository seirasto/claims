package claim;

import java.io.*;
import java.util.*;
import java.text.*;
import blog.*;

import org.w3c.dom.*;

import edu.columbia.opinion.module.*;
import edu.columbia.opinion.io.*;
import edu.columbia.opinion.process.*;
import edu.columbia.opinion.classifications.*;
import edu.columbia.utilities.weka.InstancesProcessor;
import processing.*;

import weka.core.Instances;
import weka.classifiers.functions.Logistic;
import weka.classifiers.functions.SMO;
import weka.classifiers.trees.J48;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.converters.ArffSaver;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.evaluation.NominalPrediction;
import weka.classifiers.evaluation.Prediction;

public class ClaimGenerator {
	
	String _outputDirectory = null;
	String _modelDirectory = null;
	String _inputDirectory = null;
	
	EmoticonDictionary _emoticonDictionary;
	Contractions _contractionDictionary;	
	OpinionRunner _sentiment;
	double _threshold;
	boolean _blogBoost;
	boolean _CBtagger;
	String _sentimentModel;
	boolean _overwrite;
	boolean _socialMedia;
	boolean _normalized;
	boolean _opinion;
	boolean _question;
	boolean _balanced;
	WekaBuilder _wb;
	boolean _usePOS;
	boolean _featureSelection;
	int _size;
	boolean _useWiktionary = false;
	boolean _useWordnet = false;
	boolean _useEmoticons = false;
	boolean _domain = false;
	static boolean DEBUG = true;
	
	public static void main(String[] args) throws Exception {
		if (args.length == 0) {
			System.out.println("Missing arguments:\n BlogRunner blogDirectory (-a annotatedFile -o outputDirectory -t threshold)");
			System.exit(0);
		} 
		
		//String input = null;
		//String annotatedDirectory = null;
		ArrayList<String> trainingType = new ArrayList<String>(Arrays.asList("wikipedia")); //"lj","mturk");
		ArrayList<String> testingType = null;
		String outputTraining = "wikipedia";
		String outputTesting = null;
		String eval = null;
		String dataDirectory =  "/proj/nlp/users/sara/java/claims/data/coling/"; //"/proj/nlp/users/sara/java/sentiment/data/coling/subjective/phrase-based/";
		String runnerName = "subjectivity";
		double threshold = .65;
		boolean useCBtagger = false;
		boolean overwrite = false;
		boolean balanced = false;
		boolean useSocialMedia = false;
		boolean useWiktionary = false;
		boolean useWordnet = false;
		boolean useEmoticons = false;
		boolean useOpinion = false;
		String sentimentModel = "subjectivity.arff";
		boolean usePOS = false;
		boolean domain = false;
		boolean featureSelection = false;
		String outputDirectory = "/proj/nlp/users/sara/persuasion/claims/output/" + 
		new SimpleDateFormat("MM.dd.yy").format(new Date());
		String processingDirectory = outputDirectory;
		String modelDirectory = outputDirectory;
		
		/*String trainingFile = //"data/train-lj+mturk+mpqa-wo-em-wi-n500.arff";
			//"data/train-mpqa-wo-em-wi-n500.arff";
			"data/train-wikipedia-n100-nnp-sm.arff";*/
		int size = 0; // number of n-grams to look at
		
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-o")) {
				outputDirectory = args[++i];
			}
			else if (args[i].equals("-t")) {
				threshold = Double.valueOf(args[++i]);
			}
			else if (args[i].equals("-training")) {
				trainingType = new ArrayList<String>(Arrays.asList(args[++i].split("\\+")));
				outputTraining = args[i];
			}
			else if (args[i].equals("-testing")) {
				testingType = new ArrayList<String>(Arrays.asList(args[++i].split("\\+")));
				outputTesting = args[i];
			}
			else if (args[i].equals("-processing")) {
				processingDirectory = args[++i];
			}
			else if (args[i].equals("-data")) {
				dataDirectory = args[++i];
			}
			/*else if (args[i].equals("-trainingFile")) {
				trainingFile = args[++i];
			}*/
			else if (args[i].equals("-overwrite")) {
				overwrite = true;
			}
			else if (args[i].equals("-balanced")) {
				balanced = true;
			}
			else if (args[i].equals("-cb")) {
				useCBtagger = true;
			}
			else if (args[i].equals("-sm")) {
				useSocialMedia = true;
			}
			else if (args[i].equals("-em")) {
				useEmoticons = true;
			}
			else if (args[i].equals("-wo")) {
				useWordnet = true;
			}
			else if (args[i].equals("-wi")) {
				useWiktionary = true;
			}
			else if (args[i].equals("-op")) {
				useOpinion = true;
			}
			else if (args[i].equals("-pos")) {
				usePOS = true;
			}
			else if (args[i].equals("-n")) {
				size = Integer.valueOf(args[++i]);
			}
			else if (args[i].equals("-eval")) {
				eval = args[++i];
			}
			else if (args[i].equals("-sentiment")) {
				sentimentModel = args[++i];
			}
			else if (args[i].equals("-model")) {
				modelDirectory = args[++i];
			}
			else if (args[i].equals("-domain")) {
				domain = true;
			}
			else if (args[i].equals("-fs")) {
				featureSelection = true;
			}
 			else {
 				System.out.println(args[i]);
				System.out.println("Invalid arguments:\n BlogRunner blogDirectory (-a annotatedFile -o outputDirectory -t threshold -training (lj,mpqa,lj+mpqa))");
				System.exit(0);
			}
		}	
		
		// not used right now
		if (eval != null) outputDirectory += "/" + outputTraining + (outputTesting != null ? "-" + outputTesting : "") + "/" + runnerName + "/";
    
		ClaimGenerator claimGenerator = new ClaimGenerator(trainingType, dataDirectory, outputDirectory, modelDirectory, threshold, size, 
				useCBtagger,overwrite,useSocialMedia,useOpinion,usePOS, useEmoticons, useWiktionary, useWordnet, sentimentModel, featureSelection, domain);
				
		String name = (balanced ? "balanced-" : "") + (domain ? "domain-" : "") 
				+ Arrays.toString(trainingType.toArray()).replaceAll(",", "+") + (testingType != null ? "-" + Arrays.toString(testingType.toArray()).replaceAll(",", "+") : "");
		
		if (testingType != null){
			System.out.println("RUNNING: claim-" + name);
			Instances train = claimGenerator.train(trainingType, dataDirectory + "/train/", balanced);
			Instances test = claimGenerator.test(testingType);
			
			claimGenerator.run(train, test, trainingType, name);
		}
		// run on files for Evaluation
		else if (eval != null){
			Instances train = claimGenerator.train(trainingType, dataDirectory + "/train/", balanced);
			Classifier classifier = claimGenerator.buildClassifier(train, trainingType, name, true);
			claimGenerator.run(train, classifier, trainingType, name, eval, processingDirectory);
		}
		else {
			claimGenerator.runCV(trainingType);
		}
	}
	
	public ClaimGenerator(List<String> trainingType, String dataDirectory, String outputDirectory,
			String modelDirectory, double threshold, int size, boolean useCBtagger, boolean overwrite, 
			boolean useSocialMedia, boolean useOpinion, boolean usePOS, boolean useEmoticons, boolean useWiktionary, boolean useWordNet,
			String sentimentModel, boolean featureSelection, boolean domain ) {
		try {
			System.out.println("[ClaimGenerator " + new Date() + "] initializing...");
			_emoticonDictionary = new EmoticonDictionary();
			_contractionDictionary = new Contractions();		
			_threshold = threshold;
			_CBtagger = useCBtagger;
			_outputDirectory = outputDirectory;
			_inputDirectory = dataDirectory;
			_modelDirectory = modelDirectory;
			_overwrite = overwrite;
			_normalized = true;
			_socialMedia = useSocialMedia;
			_opinion = useOpinion;
			_question = true;
			_usePOS = usePOS;
			_useWordnet = useWordNet;
			_useWiktionary = useWiktionary;
			_useEmoticons = useEmoticons;
			_sentimentModel = sentimentModel;
			_size = size;
			_domain = domain;
			_featureSelection = featureSelection;
			_wb = new WekaBuilder(_modelDirectory,
					_overwrite, _question, _socialMedia, _normalized, _CBtagger, _opinion, _size, _usePOS, _featureSelection, _domain ? toDomain(trainingType) : null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*public void runBaseline() {
		System.out.println("[ClaimGenerator " + new Date() + "] create baseline runner...");
		
		try {
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			_sentiment = SubjectivityBaselineRunner.readDAL(prop.getProperty("DAL"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		

	}*/
	
	/**
	 * Generate claims for each file in the directory
	 * 
	 * @param blogDirectory
	 * @param annotatedDirectory
	 * @throws Exception
	 */
	public void run(Instances train, Classifier classifier, ArrayList<String> trainingType, 
			String blogDirectory, String name, String processingDirectory) throws Exception {
	
		//3. run on xml test files
		List<String> files = getXMLFiles(blogDirectory);
		
		for (String file : files) {
			System.err.println("[ClaimGenerator.run] Running claim detector on " + file);
			//File f = new File(file);
			
			ArrayList <String> textTest = new ArrayList<String> ();
			ArrayList <String> chunksTest = new ArrayList<String> ();
			ArrayList <Claim> testClaims = this.processFile(file,processingDirectory,textTest,chunksTest, name);
			
			String fullProcessingPath = processingDirectory + "/processed-" + new File(file).getName() + "/" + new File(file).getName();
			
			OpinionRunner.preprocess(fullProcessingPath + ".txt", false);
			
			List<ResultSentence> opinionsTest = runOpinion(fullProcessingPath + ".txt");
			
			ArrayList<Prediction> predictions = classify(train, classifier, trainingType, testClaims,_opinion ? opinionsTest : null, name, true);    
			
			// output
			toLJ(fullProcessingPath + ".claims",fullProcessingPath + ".sentences",predictions);
			System.gc();
		}
	}
	
	public List<ResultSentence> runOpinion(String fullProcessingPath) {
		List<ResultSentence> opinions = new ArrayList<ResultSentence>();
		
		if (_opinion) {
			if (_sentiment == null) {
				_sentiment = new OpinionRunner(_sentimentModel,_useWordnet, _useWiktionary, _useEmoticons, _socialMedia, 100, false);
			}
			
			if (!new File(fullProcessingPath + ".sbj").exists()) {
				TestDataProcessor processor = new SubjectivityDataProcessor(new OpinionClassification(),true,100, false);
				
		    	AbstractDataReader reader = _sentiment.getReader(fullProcessingPath + ".emo.phrase.emo", Classification.SUBJECTIVITY, true);
		    	ArrayList<SentimentResult> subjectivity = _sentiment.test(_sentiment.getTrainingModel(true), processor, reader);

				SentimentResult.writePhrasesToFile(fullProcessingPath, subjectivity, true, _threshold);
				opinions = SentimentResult.getSentences(subjectivity);
				
			}
			else {
				
				List<String> sentences = Utils.readLines(fullProcessingPath + ".sbj");
				
				for (String sentence : sentences)
					opinions.add(SentimentResult.ProcessLabeledSentence(sentence));
			}
		}
		
		return opinions;
	}

	public ArrayList<Claim> loadAnnotations(String type, String trainingDirectory) {
		
		// read in files
		ArrayList<String> beliefSentences = new ArrayList<String>();
		List<String> claimsText = Utils.readLines(trainingDirectory + "/" + type + "_claim.csv");
		List<String> text = Utils.readLines(trainingDirectory + "/" + type + "_text.csv");
		List<String> annotation = new ArrayList<String>();
		if (_opinion && new File(trainingDirectory + "/" + type + "_annotation.csv").exists()) 
			annotation = Utils.readLines(trainingDirectory + "/" + type + "_annotation.csv");
		System.out.println("[ClaimGenerator.loadAnnotations] Preprocessing ...");
		OpinionRunner.preprocess(trainingDirectory + "/" + type + "_text.csv",false);
		List<String> pos = Utils.readLines(trainingDirectory + "/" + type + "_text.csv" + ".emo.pos");

		// committed belief tagger (optional)
		if (_CBtagger) {
			System.out.println("[ClaimGenerator.loadAnnotations] Adding committed belief ...");
			beliefSentences.addAll(Belief.tagBelief(trainingDirectory + "/" + type + "_text.csv.emo"));
		}
		
		String domain = toDomain(type);
		
		// process claims
		ArrayList <Claim> claims = new ArrayList<Claim> ();
		
		for (int index = 0; index < text.size(); index++) {
			if (index >= claimsText.size() || claimsText.get(index).isEmpty()) {
				text.remove(index);
				if (_opinion) annotation.remove(index);
				pos.remove(index);
				if (_CBtagger) beliefSentences.remove(index);
				if (index < claimsText.size()) claimsText.remove(index);
				index--;
				continue;
			}

			claims.add(new Claim("id_" + index,text.get(index),claimsText.get(index).equals("Y") ? true : false, 
					_opinion && !annotation.isEmpty() ? annotation.get(index) : null, 
							_CBtagger ? new Belief(beliefSentences.get(index)) : null, pos.get(index), domain));			
		}
		return claims;
	}
	
	private String toDomain(String type) {
		if (type.indexOf("livejournal") >= 0) return "livejournal";
		if (type.indexOf("wikipedia") >= 0) return "wikipedia";
		return type;
	}
	
	private List<String> toDomain(List<String> types) {
		for (int index = 0; index < types.size(); index++) {
			types.set(index, toDomain(types.get(index)));
		}
		return types;
	}
	
	public Classifier buildClassifier(Instances train, ArrayList<String> trainingType, String type, boolean save) {
		
		if (_domain) {
			train = domain(train, trainingType);
		}
		Classifier classifier = getClassifier("logistic");
		
		
    	try {
			//HashSet<String> experimentList = experimentList();
			String experiment = getExperimentName();
			
			if (new File(_modelDirectory + "/" + type + "-" + experiment + ".model").exists() && !_overwrite) {
				System.out.println("[ClaimGenerator.Classify " + 
						new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()) + "] Loading model: " + _modelDirectory + "/" + type + "-" + experiment + ".model");
				// load model 
				ObjectInputStream ois = new ObjectInputStream(
		                 new FileInputStream(_modelDirectory + "/" + type + "-" + experiment + ".model"));
				classifier = (Logistic) ois.readObject();
				ois.close();
			}
			else {
				classifier.buildClassifier(train);
				
				// save model
				if (save) {
					System.out.println("[ClaimGenerator.Classify " + 
						new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()) + "] Saving model to: " + _modelDirectory + "/" + type + "-" + experiment + ".model");
					new File(_modelDirectory + "/" + type + "-" + experiment + ".model").getParentFile().mkdirs();
					ObjectOutputStream oos = new ObjectOutputStream(
		                new FileOutputStream(_modelDirectory + "/" + type + "-" + experiment + ".model"));
					oos.writeObject(classifier);
					oos.flush();
					oos.close();
				}
			}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return classifier;
	}
	
	public void run(Instances train, Instances test, ArrayList<String> trainingType, String name) throws Exception {
		
		// classify
		System.out.println("[ClaimGenerator.run " + new Date() + "] Testing... ");
	
		Hashtable<String,Evaluation> classifications = new Hashtable<String,Evaluation>();
		
		List <String> experiments = new ArrayList<String>(experimentList());
		int counter = experiments.size();	
		
		classify(train, test, trainingType, classifications);
		
		String accuracy = "Accuracy, F-Score\n";
		
		new File(_outputDirectory + "/statsig/").mkdirs();
		System.out.println("Writing statistical significance to: " + _outputDirectory + "/statsig/");
		BufferedWriter statsig = new BufferedWriter(new FileWriter(_outputDirectory + "/statsig/claim-" + name));
		for (int index = 0; index < Math.pow(2,counter); index++) {
			
			String experiment = Integer.toBinaryString(index);
			while (experiment.length() < counter) experiment = "0" + experiment;
		
			String experimentName = "claim";
			
			for (int i = 0; i < experiments.size(); i++) {
				if (experiment.charAt(i) == '1')
					experimentName += experiments.get(i);
			}
		
			Evaluation eval = classifications.get(experimentName);
			
			ArrayList<Prediction> predictions = eval.predictions();
			
			statsig.write(experimentName + ",");
			for (int i = 0; i < predictions.size(); i++) {
				NominalPrediction prediction = (NominalPrediction)predictions.get(i);
				statsig.write((prediction.actual() == prediction.predicted() ? "1" : "0") + ",");
			}
			
			statsig.write("\n");
			System.out.println(experimentName);
			System.out.println(eval.toClassDetailsString());
			accuracy += experimentName + "," + eval.pctCorrect() + "," + eval.weightedFMeasure() + "\n";
		}
		statsig.close();

		System.out.println("RUNNING: claim-" + name);
		System.out.println(accuracy);
	}	
	
	public void runCV(ArrayList<String> trainingType) throws Exception {
		
		ArrayList<Claim> claims = new ArrayList<Claim> ();
		ArrayList<ResultSentence> opinions = new ArrayList<ResultSentence>();
				
		for (int index = 0; index < trainingType.size(); index++) {
			System.out.println("[ClaimGenerator.runCV] Loading " + trainingType.get(index));

			
			String file  = _inputDirectory + "/" + trainingType.get(index) + "_text.csv";
			List<ResultSentence> o = runOpinion(file);
			
			ArrayList<Claim> c = loadAnnotations(trainingType.get(index), _inputDirectory + "/train/");
			
			if (_balanced) {
				System.out.println("[ClaimGenerator.runCV] Balancing dataset ...");
				balance(c, o);
			}
			claims.addAll(c);
			opinions.addAll(o);
		}
		
		System.out.println("[ClaimGenerator.runCV " + new Date() + "] Setting up CrossValidation... ");
		
		// run training and testing
		double testSize = (double)(claims.size() * .10);
		Hashtable<String,Evaluation> classifications = new Hashtable<String,Evaluation>();
		
		List <String> experiments = new ArrayList<String>(experimentList());
		int counter = experiments.size();
		
		int run = 0;
		//for (int run = 0; run < 10; run++) {
			System.out.print("[ClaimGenerator.RunCV " + new Date() + "] Fold " + (run + 1) + "... ");

			shuffle(run, claims, opinions);

			// read in sentences
			int fold = 0;
			//for (int fold = 0; fold < 10; fold++) {
				int start = fold * (int)testSize;
				int end = start + (int)testSize;
				System.out.print((fold + 1) + ",");

				Instances train = _wb.makeTrainDataset(subList(claims,start,end,true), opinions);
				Instances test = _wb.makeTestDataset(claims.subList(start, end), opinions);
				
				try {
					List<ArrayList<Prediction>> predictions = classify(train, test, trainingType, classifications);	
					
					if (DEBUG) {
						
						System.out.print("data,");
						
						for (int index = 0; index < Math.pow(2,counter); index++) {
							String experiment = Integer.toBinaryString(index);
							while (experiment.length() < counter) experiment = "0" + experiment;
						
							String experimentName = "claim";
							
							for (int i = 0; i < experiments.size(); i++) {
								if (experiment.charAt(i) == '1')
									experimentName += experiments.get(i);
							}
							System.out.print(experimentName + ",");
						}
						System.out.println();
						
						for (int p = start; p < end; p++) {
							
							System.out.print(claims.get(p).toString().replaceAll(",", " ") + "," + claims.get(p).getCommittedBelief().toString());
							
							for (int exp = 0; exp < predictions.size(); exp++)
								System.out.print(((NominalPrediction)predictions.get(exp).get(p)).predicted() + ",");
							System.out.println();
						}
					}
				}
				catch (Exception e) {
					System.err.println("[ClaimGenerator.runCV] " + e);
					e.printStackTrace();
				}
			//}
			System.out.println();
		//}
		String accuracy = "Accuracy, F-Score\n";
		
		for (int index = 0; index < Math.pow(2,counter); index++) {
			String experiment = Integer.toBinaryString(index);
			while (experiment.length() < counter) experiment = "0" + experiment;
		
			String experimentName = "claim";
			
			for (int i = 0; i < experiments.size(); i++) {
				if (experiment.charAt(i) == '1')
					experimentName += experiments.get(i);
			}
		
			Evaluation eval = classifications.get(experimentName);
			
			System.out.println(experimentName);
			System.out.println(eval.toClassDetailsString());
			accuracy += experimentName + "," + eval.pctCorrect() + "," + eval.weightedFMeasure() + "\n";
		}
		
		System.out.println(accuracy);
	}
		
	/**
	 * Shuffle all the arrays the same way
	 */
	private void shuffle(int run, ArrayList<Claim> claims, List<ResultSentence> opinion) {
		
		Collections.shuffle(claims,new Random(run));
		if (_opinion) {
			Collections.shuffle(opinion,new Random(run));
			if (opinion != null) Collections.shuffle(opinion,new Random(run));
		}
	}

	public Instances train(List<String> trainingType, String trainingDirectory, boolean balanced) { //, AnnotatedDataReader reader) { 
		
		ArrayList<Claim> claims = new ArrayList<Claim> ();
		ArrayList<ResultSentence> opinions = new ArrayList<ResultSentence>(); 
		
		for (int index = 0; index < trainingType.size(); index++) {
			System.out.println("[ClaimGenerator.run] Loading " + trainingType.get(index));
			
			String file = trainingDirectory + trainingType.get(index) + "_text.csv";
			OpinionRunner.preprocess(file,false);
			
			List<ResultSentence> o = runOpinion(file);
			
			ArrayList<Claim> c = loadAnnotations(trainingType.get(index), trainingDirectory);
			
			if (balanced) {
				System.out.println("[ClaimGenerator.run] Balancing dataset ...");
				balance(c, o);
			}
			claims.addAll(c);
			opinions.addAll(o);
		}
		
		System.out.println("[ClaimGenerator.run " + new Date() + "] Training... ");
		
		return _wb.makeTrainDataset(claims, opinions);
	}
	
	public Instances test(ArrayList<String> testing) {
		ArrayList<Claim> claims = new ArrayList<Claim> ();
		List<ResultSentence> opinions = new ArrayList<ResultSentence>();

		for (int index = 0; index < testing.size(); index++) {

			claims.addAll(loadAnnotations(testing.get(index), _inputDirectory + "/test"));
			
			String file = _inputDirectory + "/test/" + testing.get(index) + "_text.csv";
			OpinionRunner.preprocess(file,false);
			
			opinions.addAll(runOpinion(file));
		}
		return _wb.makeTestDataset(claims, opinions);
	}
	
	public String getExperimentName() {
		String experiment = "claim-?";
		
		if (_opinion) { experiment += "-op"; }
		if (_CBtagger) { experiment += "-be";}
		if (_socialMedia) { experiment += "-sm";}
		if (_size > 0) { experiment += "-ngram";}
		if (_usePOS) { experiment += "-pos";}	
		return experiment;
	}
	
	public static Classifier getClassifier(String classifier)  {
		if (classifier.equals("nb")) return new NaiveBayes();
		if (classifier.equals("j48")) return new J48();
		//if (classifier.equals("id3")) return new Id3();
		if (classifier.equals("svm")) return new SMO();
		if (classifier.equals("logistic")) return new Logistic();
		return null;
	}
	
	public HashSet<String> experimentList() {
		HashSet<String> experimentList = new HashSet<String>();
		if (_question) {experimentList.add("-?"); }
		if (_opinion) { experimentList.add("-op"); }
		if (_CBtagger) { experimentList.add("-be"); }
		if (_socialMedia) { experimentList.add("-sm"); }
		if (_size > 0) { experimentList.add("-ngram"); }
		if (_usePOS) { experimentList.add("-pos"); }		
		return experimentList;
	}
	
	public ArrayList<Prediction> classify(Instances train, Classifier classifier, ArrayList<String> trainingType, List<Claim> claims, List<ResultSentence> opinion, String type, boolean save) {
		try {
			ArrayList<Prediction> evaluations = new ArrayList<Prediction>();
    		Instances test = _wb.makeTestDataset(claims, opinion);
    		
    		if (_domain) {
    			test = domain(test, trainingType);
			}
    		
    		Evaluation eval = new Evaluation(train);
    		eval.evaluateModel(classifier,test);
    		evaluations.addAll(eval.predictions());

    		return evaluations;
    	} catch (Exception e) {
    		System.err.println("[ClaimGenerator.classify]: " + e);
    		e.printStackTrace();
    	}
    	return null;
	}
	
	public void arffSaver(String output, Instances instances) {
		try {
			ArffSaver saver = new ArffSaver();
			saver.setInstances(instances);
			saver.setFile(new File(output));
			saver.writeBatch();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<ArrayList<Prediction>> classify(Instances fullTrain, Instances fullTest, ArrayList<String> trainingType, Hashtable<String,Evaluation> classifications) {
		
		Classifier classifier = getClassifier("logistic");
		ArrayList<ArrayList<Prediction>> evaluations = new ArrayList<ArrayList<Prediction>>();
 
		
    	try {
			ArrayList<String> experiments = new ArrayList<String>(experimentList());
			int counter = experiments.size();
    		
    		if (_domain) {
				fullTrain = domain(fullTrain, trainingType);
				fullTest = domain(fullTest, trainingType);
			}
    		
			for (int index = 0; index < Math.pow(2, counter); index++) {
				String experiment = Integer.toBinaryString(index);
				while (experiment.length() < counter) experiment = "0" + experiment;
			
				String experimentName = "claim";
				HashSet<String> experimentList = new HashSet<String>();
				
				for (int i = 0; i < experiments.size(); i++) {
					if (experiment.charAt(i) == '1') {
						experimentName += experiments.get(i);
						experimentList.add(experiments.get(i));
					}
				}
				
	    		// Let's save time! We have loaded everything, so now lets run on different sets of features to find out which are best.
	    		Instances train = InstancesProcessor.subset(fullTrain,experimentList,experiments);
	    		train.setClass(train.attribute("claim"));
	    		Instances test = InstancesProcessor.subset(fullTest,experimentList, experiments);
	    		//arffSaver(_modelDirectory + "/" + experimentName + ".arff", train);
	    		//arffSaver(_modelDirectory + "/" + experimentName + ".arff", test);
	    		classifier.buildClassifier(train);
	    		
	    		Evaluation eval = new Evaluation(train);
	    		
	    		eval.evaluateModel(classifier,test);
	    		evaluations.add(eval.predictions());

	    		classifications.put(experimentName,eval);
	    		/*System.out.println(eval.toSummaryString());
	    	    System.out.println(eval.toClassDetailsString());
	    	    System.out.println(eval.toMatrixString());*/
    		}
			
    		return evaluations;
    	} catch (Exception e) {
    		System.err.println("[ClaimGenerator.classify]: " + e);
    		e.printStackTrace();
    	}
    	return null;
	}
	
	private Instances domain(Instances instances, ArrayList<String> types) {
		try {
			System.out.print("[WekaBuilder.classify] Applying domain adaptation...");
			
			HashSet<String> exclude = new HashSet<String>();
			exclude.add("domain");
			exclude.add("claim");
			
			instances = edu.columbia.utilities.weka.DomainAdaptation.easyDomainAdaptation(instances, types, exclude);
			edu.columbia.utilities.weka.InstancesProcessor.reorder(instances , instances.attribute("claim").index());
			
			//arffSaver(outputDir + "/influence-domain-train.arff",train);
			//arffSaver(outputDir + "/influence-domain-test.arff",test);
			
			System.out.println(" DONE");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return instances;
	}
	
	public void balance(List<Claim> claims, List<ResultSentence> opinion) {
		
		int n = 0;
		int y = 0;

		for (int index = 0; index < claims.size(); index++) {
			if (claims.get(index).isClaim()) y++;
			else n++;
		}
		
		System.out.println("Y/N: " + y + "/" + n);
		
		if (y > n) y = n;
		else n = y;
		
		int yAdd = 0;
		int nAdd = 0;
		
		for (int index = 0; index < claims.size(); index++) {
		
			if ((claims.get(index).isClaim() && yAdd < y)
					|| (!claims.get(index).isClaim() && nAdd < n)) { 
				if (claims.get(index).isClaim()) yAdd++;
				else nAdd++;
			}
			else {
				claims.remove(index);
				if (_opinion && opinion != null) {
					opinion.remove(index);
				}
				index--;
			}
		}
		System.out.println("Claims: " + claims.size());
		
		for (int index = 0; index < claims.size(); index++) {
			claims.get(index).setId("id_" + index);
		}
	}
	
	/**
	 * Preprocessing
	 * @param fileName
	 * @return
	 */
	public ArrayList<Claim> processFile(String fileName, String processingDirectory, List<String> text, List<String> chunks, String type) {
		ArrayList<Claim> claims = new ArrayList<Claim>();
		
		File f = new File(fileName);
		
		//String processedFile = f.getParentFile().getParent() + "/temp/processed/processed-" + f.getName() + "/" + f.getName();
				
		String processedFile = processingDirectory + "/processed-" + f.getName() + "/" + f.getName();
		
		if (!new File(processedFile + ".txt").exists() && !new File(processedFile + ".sentences").exists()) {
			XMLUtilities.addSentenceTagsToBlogs(fileName,processedFile + ".sentences",_emoticonDictionary,_contractionDictionary);
			Document entry = XMLUtilities.getDocument(processedFile + ".sentences");
			List<String> sentences = XMLUtilities.getSentences(entry);
			processedFile = XMLUtilities.writeSentencesToTxtFile(processedFile,sentences);
		}
		else {
			processedFile += ".txt";
		}
		
		OpinionRunner.preprocess(processedFile, false);
		
		// committed belief tagger (optional)
		List <String> beliefSentences = null;
		if (_CBtagger) {
			System.err.println("[ClaimGenerator.processFile] Adding committed belief ...");
			beliefSentences = Belief.tagBelief(processedFile + ".emo");
		}
		
		List<String> chunkFile = Utils.readLines(processedFile + ".emo.pos.chk.emo");
		List<String> sentences = Utils.readLines(processedFile);
		
		for (int index = 0; index < sentences.size(); index++) {
			claims.add(new Claim(null,sentences.get(index),null ,
					_CBtagger && beliefSentences.size() > index ? new Belief(beliefSentences.get(index)) : null,
							chunkFile.get(index), type));
			chunks.add(chunkFile.get(index));
			text.add(sentences.get(index));
		}
		return claims;
	}
	
	/**
	 * Get all xml files in the directory
	 * @param directory
	 * @return
	 */
	public ArrayList<String> getXMLFiles(String directory) {
		
		if (directory == null) return null;
		
		ArrayList<File> directories = new ArrayList<File>();
		directories.add(new File(directory));
		
		ArrayList<String> files = new ArrayList<String>();
		
		FileFilter fileFilter = new FileFilter() {
		    public boolean accept(File file) {
		        if (file.toString().indexOf("/processed") >= 0) return false;
		        if (file.toString().endsWith(".txt")) return false;
		        return true;
		    }
		};
		
		while (!directories.isEmpty()) {
			
			File d = directories.remove(0);
			if (d.isFile() ) {
				files.add(d.toString());
				continue;
			}
			
			File [] directoryFiles = d.listFiles(fileFilter);
			
			for (File dF : directoryFiles)
				if (dF.isDirectory()) directories.add(dF);
				else files.add(dF.toString());
		}
		return files;
	}
	
	private List<Claim> subList(List<Claim> claims, int start, int end, boolean exclude) {
		List<Claim> subList = new ArrayList<Claim> ();
		
		if (exclude) {
			for (int index = 0; index < start; index++) {
				subList.add(claims.get(index));
			}
			for (int index = end; index < claims.size(); index++) {
				subList.add(claims.get(index));
			}
		}
		else {
			for (int index = start; index < end; index++) {
				subList.add(claims.get(index));
			}
		}
		return subList;
	}
	
	public void toLJ(String outputFile, String file, ArrayList<Prediction> predictions) {
		Document entry = XMLUtilities.getDocument(file);
		NodeList sentences = entry.getElementsByTagName("sentence");
		
		//HashSet<String> subjectiveSentences = getSubjectiveSentences(threshold);
		
		int claimId = 1;
		
		for (int index = 0; index < sentences.getLength(); index++) {
			Text sentence = (Text)sentences.item(index).getFirstChild();

			if (((NominalPrediction)predictions.get(index)).predicted() == 0) {
				
				Element subjective = entry.createElement("claim");
				subjective.appendChild(sentence);
				subjective.setAttribute("id", Integer.valueOf(claimId).toString());
				sentences.item(index).appendChild(subjective);
				claimId++;
			}
		}
		// write xml file with claims
		XMLUtilities.WriteToFile(outputFile, entry);
	}
}