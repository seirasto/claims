package claim;

/*import java.io.BufferedWriter;
//import java.io.File;
import java.io.FileWriter;
import java.util.*;

import org.w3c.dom.*;

import blog.XMLUtilities;
import sentiment.app.SentimentResult;*/

/**
 * A claim is part of persuasion. It is computed as sentiment+belief
 * @author sara
 *
 */
public class Claims {
	/*
	//List <SentimentResult> _sentiment;
	//List<Belief> _committedBelief;
	List<Claim> _claims;
	//Document _entry;
	String _fileID;
	
	public Claims(String id, Document entry, List<SentimentResult> sentiment, List<String> committedBelief) {
		//_entry = entry;
		//_sentiment = sentiment;
		_claims = new ArrayList<Claim>();
		
		if (committedBelief != null && sentiment.size() != committedBelief.size()) committedBelief = 
			adjustBelief(sentiment,committedBelief);
		
		for (int index = 0; index < sentiment.size(); index++) {
			try {
				
			_claims.add(new Claim(id,sentiment.get(index).getText(),
					sentiment.get(index),
					committedBelief == null || committedBelief.isEmpty() ? null : new Belief(committedBelief.get(index))));
			} catch (Exception e) {
				System.err.println(sentiment.get(index));
				e.printStackTrace();
			}
		}
		
		_fileID = id;
	}
	
	public Claims(String id, Document entry, List<SentimentResult> sentiment) {
		this(id,entry,sentiment,null);
	}
	
	public List<String> adjustBelief(List<SentimentResult> sentiment, List<String> committedBelief) {
		
		List<String> newCB = new ArrayList<String>();
		
		int pos = 0;
		
		for (int index = 0; index < sentiment.size(); index++) {
			
			String cbClean = committedBelief.get(pos).replaceAll("/B-[A-Z]* ", " ");
			String s = sentiment.get(index).getText().length() <= 10 ? sentiment.get(index).getText() : sentiment.get(index).getText().substring(0,10);
			String cb = cbClean.length() <= 10 ? cbClean : cbClean.substring(0,10);
			
			if (s.equals(cb)) {
				newCB.add(committedBelief.get(pos));
				pos++;
			}
			else {
				newCB.add("");
			}
			
			// check to see if document has been adjusted
			if (newCB.size() + committedBelief.size() - pos == sentiment.size()) {
				newCB.addAll(committedBelief.subList(pos, committedBelief.size()));
				return newCB;
			}
		
		}
		return newCB;
	}
	
	/*public void evaluate(String annotatedFile, String outputDirectory) throws Exception {
		if (!new File(annotatedFile).exists()) {
			System.err.println("[BlogRunner.compareAgainstAnnotationsLJ] annotated file \"" 
				+ annotatedFile + "\" does not exist");
			return;
		}
		
		BufferedWriter out = new BufferedWriter(new FileWriter(outputDirectory + "/results.txt",true));
		
    	blog.livejournal.Blog annotatedBlog = blog.livejournal.Blog.processBlog(annotatedFile);
    	Element annotatedEntry = ((blog.livejournal.Entry)annotatedBlog.getEntry((String)annotatedBlog.getEntries().next())).getEntryElement();
    	Hashtable<Integer,Persuasion> persuasions = Persuasion.processEntryForPersuasion(annotatedEntry);	

    	for (Claim claim : _claims) {
    		out.write(annotatedFile + "\t" + claim.getSentence() + "\t" + claim.isClaim() + "\t" + claim.getConfidence());

    		Iterator it = persuasions.keySet().iterator();
    		
    		while (it.hasNext()) {
    			Object key = it.next();
    			Persuasion persuasion = persuasions.get(key);
				
				if (persuasion.compareTo(claim.getSentence())) {
					out.write("\t match");
					persuasions.remove(key);
					break;
				}
    		}
    		out.write("\n");
    	}
    	if (persuasions.size() != 0) {
    		Iterator it = persuasions.keySet().iterator();
    		
    		while (it.hasNext())
    			out.write("subjectivity not found for: " + persuasions.get(it.next()).getClaim() + "\n");
    	}
    	out.close();	
	}*/
	
	/*public void outputResults(String file, String outputDirectory) throws Exception {
		BufferedWriter out = new BufferedWriter(new FileWriter(outputDirectory + "/results.txt",true));
		
		for (Claim claim : _claims) {
    		out.write(_fileID + "\t" +  claim.getSentence() + "\t" + claim.isClaim() + "\t" + claim.getConfidence() + "\n");
		}
		out.close();
	}*/
	
/*	public void evaluateClaims(List<Claim> annotations,String file) {
		
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(file + ".out"));
			
			double claim = 0;
			
			for (int index = 0; index < _claims.size(); index++) {
				if (!annotations.get(index).isAnnotated()) continue;
				if (_claims.get(index).compareTo(annotations.get(index))) claim++;
				out.write((_claims.get(index).compareTo(annotations.get(index)) ? "Y" : "N") + " " + _claims.get(index) + ", " + (_claims.get(index).getConfidence() * 100) + "\n");
			}
			out.close();
			//System.out.println("Accuracy: " + annotations.get(0).getID() + "," + claim/_claims.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void toLJ(String outputFile, Document entry, double threshold) {
		NodeList sentences = entry.getElementsByTagName("sentence");
		
		HashSet<String> subjectiveSentences = getSubjectiveSentences(threshold);
		
		int claimId = 1;
		
		for (int index = 0; index < sentences.getLength(); index++) {
			Text sentence = (Text)sentences.item(index).getFirstChild();
			String s = web.HTML.htmlToText(sentence.getTextContent());
			if (subjectiveSentences.contains(s.replaceAll("\\s+", ""))) {
				subjectiveSentences.remove(s.replaceAll("\\s++", ""));
				sentences.item(index).removeChild(sentence);
				
				Element subjective = entry.createElement("claim");
				subjective.appendChild(sentence);
				subjective.setAttribute("id", Integer.valueOf(claimId).toString());
				sentences.item(index).appendChild(subjective);
				claimId++;
			}
		}
		
    	// write to file
		if (!subjectiveSentences.isEmpty()) {
			System.err.println(subjectiveSentences.size() + " claims not inserted for " + outputFile); 
			
			for (String s : subjectiveSentences) {
				System.err.println(s);
			}
		}
		
		// write xml file with claims
		XMLUtilities.WriteToFile(outputFile, entry);
	}
	
	public HashSet<String> getSubjectiveSentences(double threshold) {
		
		HashSet<String> subjectiveSentences = new HashSet<String>();
		
		for (Claim claim : _claims) {
			if (claim.isClaim() && claim.getConfidence() >= threshold) {
				subjectiveSentences.add(claim.getSentence().replaceAll("\\s+", ""));
			}
		}
		return subjectiveSentences;
	}
	
	public void printClaims() {		
		int count = 0;
		for (int index = 0; index < _claims.size(); index++) {
			System.out.println( _claims.get(index));
			if (_claims.get(index).isClaim()) count++;
		}
		//System.out.println("Summary: There are " + count + "/" + _claims.size() + " claims. ");
	}*/
}
